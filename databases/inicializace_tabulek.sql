create table Applicants (
	applicant_id INT identity(1,1) primary key,
	first_name NVARCHAR(50),
	last_name NVARCHAR(50),
	birth_number NVARCHAR(11),
	email NVARCHAR(50),
	phone_number NVARCHAR(20) NOT NULL
);

create table Partners (
	partner_id INT identity(1,1) primary key,
	identification_number NVARCHAR(50) NOT NULL,
	active NVARCHAR(20) NOT NULL,
	contract_validity_date DATETIME NOT NULL
);

create table Contracts (
	contract_id INT identity(1,1) primary key,
	applicant_id INT NOT NULL,
	partner_id INT NOT NULL,
	state NVARCHAR(50) NOT NULL,
	partner_meeting_date DATETIME NOT NULL,
	contract_date DATETIME,
	due_months INT NOT NULL,
	applicant_requested_loan DECIMAL NOT NULL,
	bank_offered_loan DECIMAL NOT NULL,
	note TEXT
);

alter table Contracts add constraint fk_contracts_applicants foreign key (applicant_id) references Applicants(applicant_id);
alter table Contracts add constraint fk_contracts_partners foreign key (partner_id) references Partners(partner_id);
