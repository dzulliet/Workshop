﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>
        <h2>Partners Report</h2>
        <table border="1">
          <tbody>
            <tr>
              <th>Partner ID</th>
              <th>Provided Contracts</th>
              <th>Completed Contracts</th>
              <th>Success Rate</th>
            </tr>
            <xsl:for-each select="report/partner" >
              <xsl:sort select="@id"/>
              <tr>
                <td>
                  <xsl:value-of select="@id"/>
                </td>
                <td>
                  <xsl:value-of select="providedContracts"/>
                </td>
                <td>
                  <xsl:value-of select="completedContracts"/>
                </td>
                <td>
                  <xsl:value-of select="successRate"/>
                </td>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
