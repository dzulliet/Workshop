﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>
        <h2>Age Report</h2>
        <table border="1">
          <tbody>
            <tr>
              <th>Age range</th>
              <th>Count of people</th>
            </tr>
            <xsl:for-each select="report/age" >
              <tr>
                <td>
                  <xsl:value-of select="@ageRange"/>
                </td>
                <td>
                  <xsl:value-of select="count"/>
                </td>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
