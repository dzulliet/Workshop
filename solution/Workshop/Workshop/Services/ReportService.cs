﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;
using Workshop.Models;

namespace Workshop.Services
{
    public class ReportService
    {
        private BigBankEntities context;
        private const int percent = 100;

        private XmlDocument reportDocument;
        private XmlNode declaration;
        private XmlNode report;

        private static string pathToXmlPartners = @"C:\Users\user\Pictures\reportPartners.xml";
        private static string pathToHtmlPartners = @"C:\Users\user\Pictures\reportPartners.html";
        private static string pathToXsltPartners = @"C:\Users\user\Documents\Git\Workshop\solution\Workshop\Workshop\reportPartnersXSLT.xslt";

        private static string pathToXmlAge = @"C:\Users\user\Pictures\reportAge.xml";
        private static string pathToHtmlAge = @"C:\Users\user\Pictures\reportAge.html";
        private static string pathToXsltAge = @"C:\Users\user\Documents\Git\Workshop\solution\Workshop\Workshop\reportAgeXSLT.xslt";

        private static string pathToXmlDaytime = @"C:\Users\user\Pictures\reportDaytime.xml";
        private static string pathToHtmlDaytime = @"C:\Users\user\Pictures\reportDaytime.html";
        private static string pathToXsltDaytime = @"C:\Users\user\Documents\Git\Workshop\solution\Workshop\Workshop\reportDaytimeXSLT.xslt";


        public ReportService()
        {
            context = new BigBankEntities();
        }

        public void init()
        {
            reportDocument = new XmlDocument();
            declaration = reportDocument.CreateXmlDeclaration("1.0", "utf-8", null);
            reportDocument.AppendChild(declaration);

            report = reportDocument.CreateElement("report");
            reportDocument.AppendChild(report);
        }

        public void CreateXmlPartners()
        {
            init();

            HashSet<int> partners = new HashSet<int>(context.Contracts.Select(c => c.partner_id));

            foreach (int p in partners)
            {

                int providedContractsAmount = context.Contracts.Where(con => con.partner_id == p).Count();
                double completedContractsAmount = context.Contracts.Where(con => con.partner_id == p && con.state.Equals("completed")).Count();
                double success = completedContractsAmount / providedContractsAmount * percent;
                success = Math.Round(success, 2);

                XmlNode partner = reportDocument.CreateElement("partner");
                XmlAttribute partner_id = reportDocument.CreateAttribute("id");
                partner_id.Value = p.ToString();
                partner.Attributes.Append(partner_id);
                report.AppendChild(partner);

                XmlNode providedContracts = reportDocument.CreateElement("providedContracts");
                providedContracts.AppendChild(reportDocument.CreateTextNode(providedContractsAmount.ToString()));
                partner.AppendChild(providedContracts);

                XmlNode completedContracts = reportDocument.CreateElement("completedContracts");
                completedContracts.AppendChild(reportDocument.CreateTextNode(completedContractsAmount.ToString()));
                partner.AppendChild(completedContracts);

                XmlNode successRate = reportDocument.CreateElement("successRate");
                successRate.AppendChild(reportDocument.CreateTextNode($"{success.ToString()}%"));
                partner.AppendChild(successRate);

            }

            reportDocument.Save(pathToXmlPartners);
            createHtml(pathToXmlPartners, pathToHtmlPartners, pathToXsltPartners);
        }


        public void createXmlAge()
        {
            init();
            HashSet<int> applicants = new HashSet<int>(context.Contracts.Where(con => con.state.Equals("completed")).Select(con => con.applicant_id));

            List<int> ages = new List<int>();
            int counter25 = 0;
            int counter50 = 0;
            int counterAbove50 = 0;
            string[] ageRange = new string[] { "18to25", "26to50", "51+" };

            foreach (int id in applicants)
            {
                string birthNumber = context.Applicants.First(app => app.applicant_id == id).birth_number;
                string birthyear = birthNumber.Substring(0, 2);
                int age;

                if (birthyear[0] == 0)
                {
                    birthyear = "20" + birthyear;
                    age = DateTime.Now.Year - Convert.ToInt32(birthyear);
                }
                else
                {
                    birthyear = "19" + birthyear;
                    age = DateTime.Now.Year - Convert.ToInt32(birthyear);
                }

                ages.Add(age);
            }

            foreach (int age in ages)
            {
                if (age >= 18 && age <= 25)
                {
                    counter25++;
                }
                if (age >= 26 && age <= 50)
                {
                    counter50++;
                }
                if (age >= 51)
                {
                    counterAbove50++;
                }
            }

            int[] counters = new int[3] { counter25, counter50, counterAbove50 };


            for (int i = 0; i < ageRange.Length; i++)
            {
                XmlNode ageNode = reportDocument.CreateElement("age");
                XmlAttribute rangeAttr = reportDocument.CreateAttribute("ageRange");
                rangeAttr.Value = ageRange[i];
                ageNode.Attributes.Append(rangeAttr);
                report.AppendChild(ageNode);

                XmlNode count = reportDocument.CreateElement("count");
                count.AppendChild(reportDocument.CreateTextNode(counters[i].ToString()));
                ageNode.AppendChild(count);
            }

            reportDocument.Save(pathToXmlAge);
            createHtml(pathToXmlAge, pathToHtmlAge, pathToXsltAge);
        }

        public void createXmlDaytime()
        {
            init();

            List<Contract> completedContracts = new List<Contract>(context.Contracts.Where(con => con.state.Equals("completed")).ToList());

            List<int> hours = new List<int>(completedContracts.Select(con => con.contract_date.Value.Hour).ToList());

            int morning = 0;
            int noon = 0;
            int afternoon = 0;
            int night = 0;

            foreach (int hour in hours)
            {
                if (hour >= 6 && hour <= 10)
                {
                    morning++;
                }
                else if (hour >= 11 && hour <= 14)
                {
                    noon++;
                }
                else if (hour >= 15 && hour <= 20)
                {
                    afternoon++;
                }
                else
                {
                    night++;
                }
            }

            int[] daytime = new int[4] { morning, noon, afternoon, night };
            string[] timeRange = new string[4] { "6to10", "11to14", "15to20", "night" };

            for (int i = 0; i < daytime.Length; i++)
            {
                XmlNode timeNode = reportDocument.CreateElement("time");
                XmlAttribute rangeAttr = reportDocument.CreateAttribute("timeRange");
                rangeAttr.Value = timeRange[i];
                timeNode.Attributes.Append(rangeAttr);
                report.AppendChild(timeNode);

                XmlNode count = reportDocument.CreateElement("count");
                count.AppendChild(reportDocument.CreateTextNode(daytime[i].ToString()));
                timeNode.AppendChild(count);
            }

            reportDocument.Save(pathToXmlDaytime);
            createHtml(pathToXmlDaytime, pathToHtmlDaytime, pathToXsltDaytime);
        }


        public static void createHtml(string pathToXml, string pathToHtml, string pathToXslt)
        {

            XmlReader reader = XmlReader.Create(pathToXml);
            XmlWriter writer = XmlWriter.Create(pathToHtml);

            XslCompiledTransform transform = new XslCompiledTransform();
            XsltSettings settings = new XsltSettings();
            settings.EnableScript = true;

            transform.Load(pathToXslt, settings, null);

            transform.Transform(reader, writer);
        }
    }
}