﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workshop.Models;

namespace Workshop.Services
{
    public class BigBankService
    {
        private BigBankEntities context;
        private const decimal percent = 100m;

        public BigBankService()
        {
            context = new BigBankEntities();
        }

        public int AddPartner(Partner partner)
        {
            partner.active = "active";
            context.Partners.Add(partner);
            context.SaveChanges();
            return partner.partner_id;
        }

        public void DeactivatePartnerById(int id)
        {
            bool exists_partner = context.Partners.Any(p => p.partner_id == id);

            if (!exists_partner)
            {
                throw new EntityException($"Partner id does not exist");
            }
            context.Partners.First(p => p.partner_id == id).active = "inactive";
            context.SaveChanges();
        }

        public Contract UseCalculator(Contract contract)
        {
            bool is_partner_active = context.Partners.Any(p => p.partner_id == contract.partner_id && p.active == "active");
            bool exists_partner = context.Partners.Any(p => p.partner_id == contract.partner_id);
            bool exists_applicant = context.Applicants.Any(a => a.applicant_id == contract.applicant_id);
            bool valid_numbers = (contract.due_months > 0) && (contract.applicant_requested_loan > 0);

            decimal bank_offered_loan;

            if (contract.due_months == 0)
            {
                throw new DivideByZeroException($"Invalid payment period = {contract.due_months}.");
            }
            if (!exists_applicant)
            {
                throw new EntityException($"Applicant with specified id = {contract.applicant_id} does not exist.");
            }
            if (!exists_partner)
            {
                throw new EntityException($"You are not authorized to use calculator, you are not in database of partners.");
            }
            if (!is_partner_active)
            {
                throw new EntityException($"You are not authorized to use calculator, you are not active partner.");
            }
            if (!valid_numbers)
            {
                throw new ImputException($"Due months and requested loan must be positive numbers");
            }


            bank_offered_loan = MockCalculator(contract.applicant_requested_loan, contract.partner_id);
            contract.state = "potential";
            contract.partner_meeting_date = DateTime.Now;
            contract.bank_offered_loan = bank_offered_loan;
            context.Contracts.Add(contract);
            context.SaveChanges();

            contract.monthly_payment = bank_offered_loan / contract.due_months;
            contract.interest = bank_offered_loan - contract.applicant_requested_loan;
            contract.rpsn = (((contract.monthly_payment * 12) / bank_offered_loan) * 100);

            return contract;

        }

        internal List<Contract> GetAllByState(string state)
        {
            TestStateValidity(state);
            List <Contract> list_of_contracts = context.Contracts.Where(con => con.state == state).ToList();
            return list_of_contracts;
        }

        private void TestStateValidity(string state)
        {
            state.ToLower();
            bool StateValidity = (state == "completed") || (state == "potential") || (state == "denied");
            if (!StateValidity)
            {
                throw new ImputException("State is not valid");
            }
        }

        internal void UpdateStateById(int contract_id, string state, DateTime? contract_date)
        {
            TestStateValidity(state);
            if ((state.ToLower() == "completed") && (contract_date is null)) 
            {
                throw new EntityException($"You have to set contract date if you want set state to completed");
            }

            bool exists_contract = context.Contracts.Any(con => con.contract_id == contract_id);
            if (!exists_contract)
            {
                throw new EntityException($"Contract does not exist");
            }


            Contract contract = context.Contracts.First(con => con.contract_id == contract_id);
            Applicant applicant = context.Applicants.First(app => app.applicant_id == contract.applicant_id);
            bool applicant_fully_specified = !String.IsNullOrEmpty(applicant.first_name)
                                                  && !String.IsNullOrEmpty(applicant.last_name)
                                                  && !String.IsNullOrEmpty(applicant.birth_number);
            bool contractToCompleted = (state.ToLower() == "completed");
            if (!applicant_fully_specified && contractToCompleted)
            {
                throw new EntityException($"Contract state cannot be set to completed because applicant is not fully specified");
            
            }
            contract.contract_date = contract_date;
            contract.state = state;
            context.SaveChanges();

        }

        private decimal MockCalculator(decimal loan, int partner_id)
        {
            decimal interest = (1 + partner_id / percent);
            decimal offeredLoan = loan * interest;
            return offeredLoan;
        }
    }
}