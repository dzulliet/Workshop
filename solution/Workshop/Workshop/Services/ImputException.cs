﻿using System;
using System.Runtime.Serialization;

namespace Workshop.Services
{
    [Serializable]
    internal class ImputException : Exception
    {
        public ImputException()
        {
        }

        public ImputException(string message) : base(message)
        {
        }

        public ImputException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ImputException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}