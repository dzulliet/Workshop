﻿using System.IO;
using Workshop.Models;

namespace Workshop.Controllers
{
    internal class IdDocumentPair
    {

        public IdDocumentPair(int partner_id, string documentation)
        {
            this.Partner_id = partner_id;
            this.Documentation = documentation;
        }

        public string Documentation { get; set; }
        public int Partner_id { get; set; }
    }
}