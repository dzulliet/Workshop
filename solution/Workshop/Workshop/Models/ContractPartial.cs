﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Workshop.Models
{
    public partial class Contract
    {
            public decimal monthly_payment { get; set; }
            public decimal interest { get; set; }
            public decimal rpsn { get; set; }
    }
}