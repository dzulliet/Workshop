﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Workshop.Models
{
    public class Document
    {
        public string ToBinary()
        {
            byte[] bytes = File.ReadAllBytes(@"C:\doc.pdf");
            return Convert.ToBase64String(bytes);
        }

    }
}