﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Workshop.Models;
using Workshop.Services;

namespace Workshop.Controllers
{
    public class BBContractsController : ApiController
    {
        private static BigBankService bbs = new BigBankService();
        /// <summary>
        /// Call calculator with available informations, safe contract with calculated result
        /// </summary>
        /// <param name="contract">Json object that specifies contract/param>
        /// <returns>HttpResponseMessage OK/Forbidden and contract with monthly payment, interest and rps </returns>
        [HttpPost]
        public HttpResponseMessage Calculator(Contract contract)
        {
            try
            {
                bbs.UseCalculator(contract);
                return  Request.CreateResponse(HttpStatusCode.OK, contract);
            }
            catch (EntityException e)
            {
                return  Request.CreateResponse(HttpStatusCode.Forbidden,e.Message);
            }
            catch (ImputException e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Some required imput values are missing or are bad data type");
            }
        }
        /// <summary>
        /// Update state of contract by id and set time when contract was signed
        /// </summary>
        /// <param name="contract">Json object that specifies contract id, state and time when contract was signed</param>
        /// <returns>HttpResponseMessage OK/Forbidden</returns>
        public HttpResponseMessage PutState(Contract contract)
        {
            try
            {
                bbs.UpdateStateById(contract.contract_id, contract.state,contract.contract_date);
                return Request.CreateResponse(HttpStatusCode.OK, $"State was set to {contract.state}");
            }
            catch (EntityException e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, e.Message);
            }
            catch (ImputException e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Some required imput values are missing or are bad data type");
            }

        }
        /// <summary>
        /// Find all cantracts by specified state
        /// </summary>
        /// <param name="contract">Json object that specifies state of contracts</param>
        /// <returns>HttpResponseMessage OK/Forbidden and all contracts by specified state</returns>
        public HttpResponseMessage GetAllByState(Contract contract)
        {
            try
            {
                List<Contract> contracts_by_state = bbs.GetAllByState(contract.state);
                return Request.CreateResponse(HttpStatusCode.OK, contracts_by_state);
            }
            catch(ImputException e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Some required imput values are missing or are bad data type");
            }
        }
    }
}
