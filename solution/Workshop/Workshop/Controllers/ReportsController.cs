﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Workshop.Services;

namespace Workshop.Controllers
{

    public class ReportsController : ApiController
    {
        private static ReportService rs = new ReportService();
        /// <summary>
        /// Create 3 reports, each in xml and html format and saves them in your Pictures folder
        /// </summary>
        /// <returns>HttpResponseMessage OK/InternalServerError</returns>
        public HttpResponseMessage GetReports()
        {
            try
            {
                rs.CreateXmlPartners();
                rs.createXmlAge();
                rs.createXmlDaytime();
            }
            catch (DirectoryNotFoundException e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Incorrect path to XSLT file(s)");
            }
            catch(Exception e)
            {
               return Request.CreateResponse(HttpStatusCode.InternalServerError, "Not enough data to create reports");
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Report files have been created in your Pictures directory");
        }


    }
}