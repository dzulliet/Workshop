﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Workshop.Models;
using Workshop.Services;

namespace Workshop.Controllers
{
    public class BBPartnersController : ApiController
    {
        private static BigBankService bbs = new BigBankService();
        /// <summary>
        /// Post a new parter and set state to active
        /// </summary>
        /// <param name="partner">Json object thate specifies name, identification number and contract validity date</param>
        /// <returns>HttpResponseMessage OK/NotFound/Forbidden and documentation in binary</returns>
        [HttpPost]
        public HttpResponseMessage PostPartner(Partner partner)
        {
            int new_partner_id;
            try
            {
                new_partner_id = bbs.AddPartner(partner);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Some required imput values are missing or are in bad data type");
            }
            try
            {
                Document mockDocumentation = new Document();
                IdDocumentPair id_doc = new IdDocumentPair(new_partner_id, mockDocumentation.ToBinary());
                return Request.CreateResponse(HttpStatusCode.OK, id_doc);
            }
            catch (FileNotFoundException e)
            {
                IdDocumentPair id_doc = new IdDocumentPair(new_partner_id, "Documentation could not be created");
                return Request.CreateResponse(HttpStatusCode.NotFound, id_doc);
            }
        }
        /// <summary>
        /// Set partner state to inactive
        /// </summary>
        /// <param name="partner">Json object that specifies partner id</param>
        /// <returns>HttpResponseMessage Ok/NotFound/Forvidenc</returns>
        [HttpDelete]
        public HttpResponseMessage DeletePartner(Partner partner)
        {
            try
            {
                bbs.DeactivatePartnerById(partner.partner_id);
                return Request.CreateResponse(HttpStatusCode.OK, "Partner was deleted");
            }
            catch(EntityException e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Some required imput values are missing or are in bad data type");
            }

        }
       
    }
}
