﻿using System;
using Workshop;
using NUnit.Compatibility;
using Workshop.Services;
using Workshop.Models;
using NUnit.Framework;
using Workshop.Controllers;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace WorkshopTest
{
    [TestFixture]
    public class CotractTest
    {
        [Test]
        public void TestPutStateInvalidState()
        {
            var controller = new BBContractsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.PutState(new Contract() { contract_id = 16, contract_date = new DateTime(1999, 1, 1), state = "invalidstate" });
        
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));
        }

        [Test]
        public void TestPutStateCompletedWithoutContractDate()
        {
            var controller = new BBContractsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.PutState(new Contract() { contract_id = 16, state = "completed" });

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));
        }

        [Test]
        public void TestGetAllByStateInvalidstate()
        {
            var controller = new BBContractsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.PutState(new Contract() { state = "invalidstate" });

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));
        }

    }
}
