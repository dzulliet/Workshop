﻿using System;
using Workshop;
using NUnit.Compatibility;
using Workshop.Services;
using Workshop.Models;
using NUnit.Framework;
using Workshop.Controllers;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace WorkshopTest
{
    [TestFixture]
    public class PartnerTest
    {
        [Test]
        public void TestDeleteInvalidPartner()
        {
            var controller = new BBPartnersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.DeletePartner(new Partner() { partner_id = -20 });

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));
        }
      
    }
}
